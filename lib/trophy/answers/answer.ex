defmodule Trophy.Answers.Answer do
  use Ecto.Schema
  import Ecto.Changeset

  schema "answers" do
    field :number, :integer
    field :answer, :string

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(answer, attrs) do
    answer
    |> cast(attrs, [:number, :answer])
    |> validate_required([:number, :answer])
  end
end
