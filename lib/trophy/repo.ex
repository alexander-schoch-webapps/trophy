defmodule Trophy.Repo do
  use Ecto.Repo,
    otp_app: :trophy,
    adapter: Ecto.Adapters.Postgres
end
