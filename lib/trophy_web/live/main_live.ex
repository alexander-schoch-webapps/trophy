defmodule TrophyWeb.MainLive do
  use TrophyWeb, :live_view
  alias Trophy.{Answers, Answers.Answer}

  def num_questions, do: 10

  def text_input(assigns) do
    ~H"""
    <input
      id={@field.id}
      name={@field.name}
      value={@field.value}
      class={@class}
      placeholder={@placeholder}
      disabled={@disabled}
    />
    """
  end

  def get_question_array() do
    1..num_questions()
    |> Enum.reduce(
      %{},
      fn i, acc ->
        struct =
          %Answer{}
          |> Answers.change_answer()
          |> to_form()

        s = %{struct | :data => %{struct.data | :number => i}}
        key = "q#{i}" |> String.to_atom()
        Map.put(acc, key, s)
      end
    )
  end

  def mount(_params, _session, socket) do
    socket =
      assign(
        socket,
        forms: get_question_array()
      )

    {:ok, socket}
  end

  def handle_event(event, %{"answer" => params}, socket) do
    question = event |> String.replace("submit-q", "") |> String.to_integer()
    par = params |> Map.put("number", question)

    case Answers.create_answer(par) do
      {:ok, answer} ->
        forms = socket.assigns.forms
        key = "q#{answer.number}" |> String.to_existing_atom()

        updated_forms =
          forms |> Map.delete(key) |> Map.put(key, answer |> Answers.change_answer() |> to_form)

        socket =
          assign(
            socket,
            :forms,
            updated_forms
          )

        # changeset = Answers.change_answer(%Answer{})
        {:noreply, socket}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, :form, to_form(changeset))}
    end
  end
end
