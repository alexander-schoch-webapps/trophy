defmodule TrophyWeb.HostLive do
  use TrophyWeb, :live_view
  import TrophyWeb.MainLive, only: [num_questions: 0]
  alias Trophy.Answers

  @interval 3000 # Interval in milliseconds (3000 ms = 3 seconds)

  def get_max(options) do
    options
    |> Enum.reduce(
      1,
      fn x, acc ->
        if x > acc, do: x, else: acc
      end
    )
  end

  def get_answers(question) do
    Answers.list_answers() |> Enum.filter(fn elem -> elem.number === question end)
  end

  def get_data(answers, socket) do
    ans = answers |> Enum.map(fn elem -> elem.answer |> String.capitalize() end)
    unique_answers = ans |> Enum.uniq()

    data =
      unique_answers
      |> Enum.reduce(
        %{},
        fn x, acc ->
          Map.put(acc, x, ans |> Enum.filter(fn y -> y === x end) |> length)
        end
      )

    max = Map.keys(data) |> Enum.map(fn x -> data[x] end) |> get_max

    socket = assign(
      socket,
      data: Map.keys(data)
      |> Enum.reduce(
        %{},
        fn x, acc ->
          Map.put(acc, x, data[x] / max * 100)
        end
      )
    )
    assign(
      socket,
      numbers: Map.keys(data) |> Enum.reduce(%{}, fn x, acc -> Map.put(acc, x, data[x]) end)
    )
  end

  def mount(_params, _session, socket) do
    socket = assign(socket, question: 1)
    socket = assign(socket, num_questions: num_questions())

    socket = get_answers(1) |> get_data(socket)

    if connected?(socket) do
      ^socket = socket |> schedule_update()
    end

    {:ok, socket}
  end

  def handle_event("next-question", _params, socket) do
    socket = assign(socket, question: socket.assigns.question + 1)
    socket = get_answers(socket.assigns.question) |> get_data(socket)
    {:noreply, socket}
  end

  def handle_event("prev-question", _params, socket) do
    socket = assign(socket, question: socket.assigns.question - 1)
    socket = get_answers(socket.assigns.question) |> get_data(socket)
    {:noreply, socket}
  end

  def handle_event("clear-data", _params, socket) do
    answers = Answers.list_answers()
    answers |> Enum.each(fn elem -> Answers.delete_answer(elem) end)
    socket = get_answers(socket.assigns.question) |> get_data(socket)
    {:noreply, socket}
  end

  defp schedule_update(socket) do
    Process.send_after(self(), :update, @interval)
    socket
  end

  def handle_info(:update, socket) do
    socket = get_answers(socket.assigns.question) |> get_data(socket)
    {:noreply, socket |> schedule_update()}
  end
end
