defmodule Trophy.Repo.Migrations.CreateAnswers do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :number, :integer
      add :answer, :string

      timestamps(type: :utc_datetime)
    end
  end
end
