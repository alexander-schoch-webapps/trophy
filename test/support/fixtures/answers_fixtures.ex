defmodule Trophy.AnswersFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Trophy.Answers` context.
  """

  @doc """
  Generate a answer.
  """
  def answer_fixture(attrs \\ %{}) do
    {:ok, answer} =
      attrs
      |> Enum.into(%{
        answer: "some answer"
      })
      |> Trophy.Answers.create_answer()

    answer
  end

  @doc """
  Generate a answer.
  """
  def answer_fixture(attrs \\ %{}) do
    {:ok, answer} =
      attrs
      |> Enum.into(%{
        answer: "some answer",
        number: 42
      })
      |> Trophy.Answers.create_answer()

    answer
  end
end
